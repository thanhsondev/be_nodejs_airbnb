import { Controller, Get, InternalServerErrorException, HttpCode, Post, Body, Put, Param, Delete, UseGuards } from '@nestjs/common';
import { CommentService } from './comment.service';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { CommentDto } from './comment-dto/comment-dto';
import { AuthGuard } from '@nestjs/passport';


@ApiBearerAuth()
@UseGuards(AuthGuard("jwt"))
@ApiTags('Bình Luận')
@Controller('api')
export class CommentController {
  constructor(private readonly commentService: CommentService) {}

  @HttpCode(200)
  @Get('/binh-luan')
  getComment(){
    try {
      let data =this.commentService.getComment()
      return data
    } catch (error) {
      throw new InternalServerErrorException('Internet Server Error')
    }
  }

  @HttpCode(200)
  @Post('/binh-luan')
  createComment(
    @Body() commentType: CommentDto
  ){
    try {
      let data = this.commentService.createComment(commentType)

      return data
    } catch (error) {
      throw new InternalServerErrorException('Internet Server Error')
    }
  }


  @HttpCode(200)
  @Put('/binh-luan/:id')
  updateComment(
    @Body() commentUpdate: CommentDto,
    @Param('id') id:string
  ){
    try {
      return this.commentService.updateComment(commentUpdate, id)
    } catch (error) {
      throw new InternalServerErrorException('Internet Server Error')
    }
  }

  @HttpCode(200)
  @Delete('/binh-luan/:id')
  deleteComment(
    @Param('id') id:string
  ){
    try {
      return this.commentService.deleteComment(id)
    } catch (error) {
      throw new InternalServerErrorException('Internet Server Error')
    }
  }


  @Get('/binh-luan/lay-binh-luan-theo-phong/:maPhong')
  getCommentByRoom(
    @Param('maPhong') maPhong:string
  ){
    try {
      let data = this.commentService.getCommentByRoom(maPhong)
      return data
    } catch (error) {
      throw new InternalServerErrorException('Internet Server Error')
    }
  }
}
