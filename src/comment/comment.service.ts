import { Injectable, NotFoundException } from '@nestjs/common';
import { PrismaClient } from '@prisma/client';
import { CommentDto } from './comment-dto/comment-dto';

@Injectable()
export class CommentService {

    prisma = new PrismaClient()

    async getComment(){
        let data = await this.prisma.binhLuan.findMany()
        return data
    }

    async createComment(commentType: CommentDto){
        let {ma_phong, ma_nguoi_binh_luan,noi_dung,ngay_binh_luan, sao_binh_luan} = commentType

        let newData = {
            ma_phong,
            ma_nguoi_binh_luan,
            noi_dung,
            ngay_binh_luan,
            sao_binh_luan
        }

        let data = this.prisma.binhLuan.create({data: newData})

        return data
    }

    async updateComment(commentUpdate: CommentDto, id:string){
        let findComment = await this.prisma.binhLuan.findFirst({
            where: {
                id: Number(id)
            }
        })

        if(!findComment){
            throw new NotFoundException("Không tìm thấy bình luận")
        }

        let updateData = await this.prisma.binhLuan.update({
            where: {
                id: Number(id)
            },
            data: commentUpdate
        })
        return updateData
    }

    async deleteComment(id:string){
        return await this.prisma.binhLuan.delete({
            where: {
                id: Number(id)
            }
        })
    }

    async getCommentByRoom(maPhong: string){
        let data = this.prisma.phong.findMany({
            where: {
                id: Number(maPhong)
            },
            include: {
                binhLuan: true
            }
        })

        return data
    }


}
