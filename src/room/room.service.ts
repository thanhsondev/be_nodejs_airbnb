import { Injectable, NotFoundException, Param, Req, Res } from '@nestjs/common';
import { PrismaClient } from '@prisma/client';
import { roomType } from './room-dto/room.dto';
import { LoginType } from 'src/auth/dto/create-auth.dto';
import { NotFoundError } from 'rxjs';

@Injectable()
export class RoomService {
    prisma = new PrismaClient()

   async getRoom(
        @Res() res
    ){
        let data = await this.prisma.phong.findMany()
        res.json(data)
    }

    async getRoomByLocation( viTri: string,  @Res() res){
        let data = await this.prisma.phong.findMany({
            where:{
                viTri: {
                    ten_vi_tri:viTri
                }
            },
        })
        res.json({data:data})
        return data
    }


    async getRoomPagination(pageIndex: number = 1, pageSize: number = 10, keyword: string) {
            let dataPhong = await this.prisma.phong.findMany({
                where:{
                    ten_phong: {
                        contains: keyword
                    }
                },
                skip: ((pageIndex - 1) * pageSize),
                take: pageSize
            })
            return dataPhong

    }


    async getRoomById(id: number){
        let data =await this.prisma.phong.findUnique({
            where: {
                id: Number(id)
            }
        })
        return data
    }

    async createRoom(roomType: roomType, res){
        let {ban_la, ban_ui,bep,dieu_hoa,do_xe,gia_tien,giuong,hinh_anh,ho_boi,khach, may_giat,mo_ta,phong_ngu,phong_tam,ten_phong,tivi,viTri_id,wifi, nguoiDung_id} = roomType
        
        let newData = {
            ten_phong,
            khach,
            phong_ngu,
            giuong,
            phong_tam,
            mo_ta,
            gia_tien,
            may_giat,
            ban_la,
            tivi,
            dieu_hoa,
            wifi,
            bep,
            do_xe,
            ho_boi,
            ban_ui,
            hinh_anh,
            viTri_id ,
            nguoiDung_id: 1
        }

       await this.prisma.phong.create({data:newData})
        
        res.json({mess:"Tạo thành công"})
     
    }


    async uploadImgRoom(roomId: number, filePath: string) {
        await this.prisma.phong.update({
            where: {
                id: roomId
            },
            data: {
                hinh_anh: filePath
            }
        })
    }

    async deleteRoom(id:string){
        await this.prisma.phong.delete({
            where: {
                id: Number(id)
            }
        })
    }

    async updateRoom(id: string, roomType:roomType){
        const updateRoom = await this.prisma.phong.findUnique({
            where: {
                id: Number(id)
            }
        })

        if(!updateRoom) {
             throw new NotFoundException("Mã phòng không tìm thấy")
        }

        return this.prisma.phong.update({
            where: {
                id: Number(id)
            },
            data: roomType
        })

    }


    
}
