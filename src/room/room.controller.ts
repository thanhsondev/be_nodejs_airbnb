import { Controller, Get, InternalServerErrorException, Res, HttpCode, UseGuards, Headers, Header, Param, Body, Query, DefaultValuePipe, ParseIntPipe, Post, UseInterceptors, UploadedFile, Delete, Put } from '@nestjs/common';
import { RoomService } from './room.service';
import {ApiHeader, ApiTags, ApiBearerAuth, ApiQuery} from '@nestjs/swagger'
import { AuthGuard } from '@nestjs/passport';
import { roomType } from './room-dto/room.dto';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';

@ApiBearerAuth()
@UseGuards(AuthGuard("jwt"))
@ApiTags("Phong")
@Controller('room')
export class RoomController {
  constructor(private readonly roomService: RoomService) {}



  @HttpCode(200)
  @Get("/phong-thue")
  @ApiHeader({name:"Authorization", description:"Bearer "})
  getRoom(@Res() res) {
    try {
      let data = this.roomService.getRoom(res)
      return data
    } catch (error) {
      throw new InternalServerErrorException("Internet Server Error")
    }
  }

  

  @HttpCode(200)
  @Get("/phong-thue/lay-phong-theo-vi-tri/:viTri")
  getRoomByLocation(
    @Param("viTri") viTri: string,
    @Res() res
    ){
    try {
      let data = this.roomService.getRoomByLocation(viTri, res)
      return data
    } catch (error) {
      throw new InternalServerErrorException("Internet Server Error")
    }
  }


  @ApiQuery({
    name:'keyword',
    required: false,
    type: String,
    description: "The keyword for searching"
  })
  @HttpCode(200)
  @Get("/phong-thue/phan-trang-tim-kiem")
  getRoomPagination(
    @Query('pageIndex', new DefaultValuePipe(1), ParseIntPipe) pageIndex: number,
    @Query('pageSize', new DefaultValuePipe(10), ParseIntPipe) pageSize: number,
    @Query('keyword') keyword: string,
  ){
    try {
      let data = this.roomService.getRoomPagination(pageIndex, pageSize, keyword)
      return data
    } catch (error) {
      throw new InternalServerErrorException("Internet Server Error")
    }
  }



  @HttpCode(200)
  @Get('/phong-thue/:id')
  getRoomById(@Param('id') id: number) {
    try {
      let data = this.roomService.getRoomById(id)
      return data
    } catch (error) {
      throw new InternalServerErrorException("Internet Server Error")
    }
  }



  @Post('phong-thue')
  createRoom(@Body() roomType: roomType, @Res() res){
    try {
      return this.roomService.createRoom(roomType, res)
    } catch (error) {
      throw new InternalServerErrorException("Internet Server Error")
    }
  }


  @UseInterceptors(FileInterceptor('file', {
    storage: diskStorage({
      destination:process.cwd() + "/public/img",
      filename: (req, file, callback) => callback(null, new Date().getTime() + "_" + file.originalname)
    })
  }))
  @Post('phong-thue/:maPhong/upload-hinh-phong')
  async uploadImgRoom(@UploadedFile() file:Express.Multer.File, @Param('maPhong') roomId: string){
    try {

      const filePath = file.path
      await this.roomService.uploadImgRoom(Number(roomId), filePath)
      return {filename: file.filename, path: filePath, roomId}
    } catch (error) {
      throw new InternalServerErrorException("Internet Server Error")
    }
  }
  

  @Delete('/phong-thue/:id')
  deleteRoom(@Param('id') id:string) {
    try {
      this.roomService.deleteRoom(id)
    } catch (error) {
      throw new InternalServerErrorException("Internet Server Error")
    }
  }


  @Put('phong-thue/:id')
  updateRoom(@Param('id') id:string, @Body() roomType:roomType){
    try {
      let result = this.roomService.updateRoom(id, roomType)
      return result
    } catch (error) {
      throw new InternalServerErrorException("Internet Server Error")
    }
  }
}
