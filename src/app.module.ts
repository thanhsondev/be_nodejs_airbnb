import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import {ConfigModule} from '@nestjs/config'
import { RoomModule } from './room/room.module';
import { JwtStrategy } from './strategy/jwt.strategy';
import { LocationModule } from './location/location.module';
import { UsersModule } from './users/users.module';
import { BookroomModule } from './bookroom/bookroom.module';
import { CommentModule } from './comment/comment.module';
@Module({
  imports: [AuthModule,
    ConfigModule.forRoot({
    isGlobal:true
  }),
    RoomModule,
    LocationModule,
    UsersModule,
    BookroomModule,
    CommentModule],
  controllers: [AppController],
  providers: [AppService, JwtStrategy],
})
export class AppModule {}
