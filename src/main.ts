import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.enableCors()
  const config = new DocumentBuilder().setTitle("Airbnb").addBearerAuth().build()
  const documnet  = SwaggerModule.createDocument(app, config)
  SwaggerModule.setup("/swagger", app, documnet)

  await app.listen(8080);
}
bootstrap();
