import { Module } from '@nestjs/common';
import { BookroomService } from './bookroom.service';
import { BookroomController } from './bookroom.controller';

@Module({
  controllers: [BookroomController],
  providers: [BookroomService],
})
export class BookroomModule {}
