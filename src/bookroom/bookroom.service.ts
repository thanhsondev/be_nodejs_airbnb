import { Injectable, NotFoundException } from '@nestjs/common';
import { PrismaClient } from '@prisma/client';
import { BookRoomDto } from './bookRoom-dto/bookRoom-dto';


@Injectable()
export class BookroomService {
    prisma = new PrismaClient()


    async getListBookRoom(res){
       let data = await this.prisma.datPhong.findMany()
        res.json(data)
       return data
    }

    async createBookRoom(body: BookRoomDto){
        let {ma_phong, ma_nguoi_dat,ngay_di,ngay_den,so_luong_khach} = body

        let newData = {
            ma_phong,
            ma_nguoi_dat,
            ngay_den,
            ngay_di,
            so_luong_khach
        }

        return this.prisma.datPhong.create({data: newData})
    }

    async getBookRoomByID(id:string){
        let data = await this.prisma.datPhong.findFirst({
            where: {
                id: Number(id)
            }
        })
         return data
    }

    async updateBookRoom(id:string, body: BookRoomDto){
       let finData = await this.prisma.datPhong.findFirst({
        where: {
            id: Number(id)
        }
       })
       if(!finData){
        throw new NotFoundException("Vị trí không tìm thấy")
       }

       let updateData = await this.prisma.datPhong.update({
        where: {
            id: Number(id)
        },
        data: body
       })
       return updateData
    }

    async deleteBookRoom(id:string){
        return await this.prisma.datPhong.delete({
            where: {
                id: Number(id)
            }
        })
    }

    async getBookRoomByIdUser(maNguoiDung: string){
        let data = this.prisma.nguoiDung.findMany({
            where: {
                id: Number(maNguoiDung)
            },
            include: {
                datPhong: true
            }
        })
        return data
    }
}
