import { Controller, Get, HttpCode, InternalServerErrorException, Res, Post, Body, Param, Put, Delete, UseGuards } from '@nestjs/common';
import { BookroomService } from './bookroom.service';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { BookRoomDto } from './bookRoom-dto/bookRoom-dto';
import { AuthGuard } from '@nestjs/passport';


@ApiBearerAuth()
@UseGuards(AuthGuard("jwt"))
@ApiTags('Đặt phòng')
@Controller('api')
export class BookroomController {
  constructor(private readonly bookroomService: BookroomService) {}


  @HttpCode(200)
  @Get('/dat-phong')
  getListBookRoom(@Res() res){
    try {
      let data = this.bookroomService.getListBookRoom(res)
      return data
    } catch (error) {
      throw new InternalServerErrorException('Internet Server Error')
    }
  }

  @HttpCode(200)
  @Post('/dat-phong')
  createBookRoom(
    @Body() body: BookRoomDto
  ){
    try {
      let data = this.bookroomService.createBookRoom(body)
      return data
    } catch (error) {
      throw new InternalServerErrorException('Internet Server Error')
    }
  }

  @HttpCode(200)
  @Get('/dat-phong/:id')
  getBookRoomByID(
    @Param('id') id:string
  ){
    try {
      let data = this.bookroomService.getBookRoomByID(id)
      return data
    } catch (error) {
      throw new InternalServerErrorException('Internet Server Error')
    }
  }

  @HttpCode(200)
  @Put('/dat-phong/:id')
  updateBookRoom(
    @Param('id') id:string,
    @Body() body:BookRoomDto
  ){
    try {
      let data = this.bookroomService.updateBookRoom(id, body)
      return data
    } catch (error) {
      throw new InternalServerErrorException('Internet Server Error')
    }
  }

  @HttpCode(200)
  @Delete('/dat-phong/:id')
  deleteBookRoom(
    @Param('id') id:string,
    @Res() res
  ){
    try {
      res.json({mess: 'Xóa thành công'})
      return this.bookroomService.deleteBookRoom(id)
    } catch (error) {
      throw new InternalServerErrorException('Internet Server Error')
    }
  }


  @Get('/dat-phong/lay-theo-nguoi-dung/:maNguoiDung')
  getBookRoomByIdUser(
    @Param('maNguoiDung') maNguoiDung:string
  ){
    try {
      let data = this.bookroomService.getBookRoomByIdUser(maNguoiDung)
      return data
    } catch (error) {
      throw new InternalServerErrorException('Internet Server Error')
    }
  }
}
