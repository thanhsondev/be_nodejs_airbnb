import { Controller, Post, Body, InternalServerErrorException, Res, HttpCode , UseGuards} from '@nestjs/common';
import { AuthService } from './auth.service';
import { LoginType, RegisterType } from './dto/create-auth.dto';
import { ConfigService } from '@nestjs/config';
import {ApiBearerAuth, ApiTags} from '@nestjs/swagger'
import { AuthGuard } from '@nestjs/passport';


// @ApiBearerAuth()
// @UseGuards(AuthGuard("jwt"))
@ApiTags("Auth")
@Controller('auth')
export class AuthController {
  constructor(
    private readonly authService: AuthService,
    private configService: ConfigService
    
    ) {}

  @Post("/register")
  register(@Body() body: RegisterType, @Res() res) {
    try {
      return this.authService.register(body, res);
      
    } catch (error) {
      throw new InternalServerErrorException("Internet Server Error")
    }
  }

  @HttpCode(200)
  @Post("/login")
  login(
    @Body() body: LoginType, @Res() res) {
      try {
        this.authService.login(body, res);
        return
      } catch (error) {
        throw new InternalServerErrorException("Internet Server Error")
      }

  }
}
