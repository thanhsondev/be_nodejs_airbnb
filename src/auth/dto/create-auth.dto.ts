import { ApiProperty } from "@nestjs/swagger"

export class RegisterType {
    @ApiProperty()
    name: string
    @ApiProperty()
    email: string
    @ApiProperty()
    pass_word: string
    @ApiProperty()
    phone: string
    @ApiProperty()
    birth_day: string
    @ApiProperty()
    gender:string
    role:string
}


export class LoginType {
    @ApiProperty()
    email: string
    @ApiProperty()
    pass_word:string
}