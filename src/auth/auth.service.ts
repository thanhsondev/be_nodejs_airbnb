import { Injectable, Res } from '@nestjs/common';
import { PrismaClient } from '@prisma/client';
import * as bcrypt from 'bcrypt'
import { JwtService } from '@nestjs/jwt';
import { LoginType, RegisterType } from './dto/create-auth.dto';

@Injectable()
export class AuthService {
  constructor(
    private jwtServise: JwtService
  ){}
  
   prisma = new PrismaClient

  async register(
    InfoUser: RegisterType,
    @Res() res
  ) {

    let {name, email, pass_word, phone, birth_day, gender } = InfoUser
    const checkUser =await this.prisma.nguoiDung.findFirst({
      where:{
        email: email
      }
    })

    if(checkUser) {
      res.json({message:"Email đã tồn tại"})
      return
    } 
    let password_bcrypt = bcrypt.hashSync(pass_word, 10)
    let newdata = {
      name: name,
      email: email,
      pass_word: password_bcrypt,
      phone: phone,
      birth_day: birth_day,
      gender:gender,
      role:"User", 
      avatar:""
    }

    await this.prisma.nguoiDung.create({
    data: newdata
   })
     res.json({message:"Đăng ký thành công"})
  }


 async login(
  InfoUser: LoginType,
  @Res() res
 ) {
    let {email, pass_word} = InfoUser
    const checkUser = await this.prisma.nguoiDung.findFirst({
      where:{
        email: email
      }
    })

    if(checkUser){
      let checkPass = bcrypt.compareSync(pass_word, checkUser.pass_word)
      if(checkPass){
        let token = this.jwtServise.sign({data:checkUser}, {
          expiresIn: "120d", secret:"BIMAT1"
        })
        res.json({data:token})
        // return token
        // res.json({mess:"Đăng nhập thành công"})
      }else {
        res.json({mess:"Mật khẩu không đúng"})
      }
    } else {
      res.json({mess:"Email không đúng"})
    }
    
  }
}
