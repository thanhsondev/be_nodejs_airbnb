import { ApiProperty } from "@nestjs/swagger"

export class UsersType {
    @ApiProperty()
    name: string
    @ApiProperty()
    email: string
    @ApiProperty()
    pass_word: string
    @ApiProperty()
    phone: string
    @ApiProperty()
    birth_day: string
    @ApiProperty()
    gender:string
    @ApiProperty()
    role:string
}

export class UploadDto {
    @ApiProperty({type: 'string', format: 'binary'})
    avatar: any
}