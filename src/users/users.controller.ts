import { Body, Controller, Get, InternalServerErrorException, Post, Res, Delete, Param, Query, DefaultValuePipe, ParseIntPipe, HttpCode, Put, UseInterceptors, UploadedFile, UseGuards } from '@nestjs/common';
import { UsersService } from './users.service';
import { ApiBearerAuth, ApiBody, ApiConsumes, ApiQuery, ApiTags } from '@nestjs/swagger';
import { UploadDto, UsersType } from './dto/user.dto';
import { get } from 'http';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { AuthGuard } from '@nestjs/passport';


@ApiBearerAuth()
@UseGuards(AuthGuard("jwt"))
@ApiTags('Người dùng')
@Controller('api')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @HttpCode(200)
  @Get('/users')
  getUsers(@Res() res){
    try {
      let data = this.usersService.getUsers(res)
      return data
    } catch (error) {
      throw new InternalServerErrorException('Internet Server Error')
    }
  }


  @HttpCode(200)
  @Post('/users')
  createUsers(@Body() body:UsersType){
    try {
      this.usersService.createUsers(body)
    } catch (error) {
      throw new InternalServerErrorException('Internet Server Error')
    }
  }


  @HttpCode(200)
  @Delete("/users")
  deleteUsers(
    @Query('id') id:string,
    @Res() res
  ){
    try {
      return this.usersService.deleteUser(id, res)
    } catch (error) {
      throw new InternalServerErrorException('Internet Server Error')
    }
  }


  @ApiQuery({
    name: 'keyword',
    required:false,
    type:String,
    description:'The keyword for searching'
  
  })
  @HttpCode(200)
  @Get("users/phan-trang-tim-kiem")
  getUsersPagination(
    @Query('pageIndex', new DefaultValuePipe(1), ParseIntPipe) pageIndex: number,
    @Query('pageSize', new DefaultValuePipe(10), ParseIntPipe) pageSize:number,
    @Query('keyword') keyword:string
  ){
    try {
      let data = this.usersService.getUsersPagination(pageIndex, pageSize, keyword)
      return data
    } catch (error) {
      throw new InternalServerErrorException('Internet Server Error')
    }
  }



  @HttpCode(200)
  @Get('users/:id')
  getUsersByID(
    @Param('id') id:string
  ){
    try {
      let data = this.usersService.getUsersByID(id)
      return data
    } catch (error) {
      throw new InternalServerErrorException('Internet Server Error')
    }
  }


  @HttpCode(200)
  @Put("/users/:id")
  updateUser(
    @Param('id') id:string,
    @Body() UsersType: UsersType
  ){
    try {
    let newData = this.usersService.updateUsers(id, UsersType)
    return newData
    } catch (error) {
      throw new InternalServerErrorException('Internet Server Error')
    }
  }


  @HttpCode(200)
  @Get('/users/search/:TenNguoiDung')
  getUserByName(
    @Query('TenNguoiDung') TenNguoiDung:string,
  ){
    try {
      let data = this.usersService.getUserByName(TenNguoiDung)
      return data
    } catch (error) {
      throw new InternalServerErrorException('Internet Server Error')
    }
  }


  @ApiConsumes('multipart/form-data')
  @ApiBody({
    description: 'UploadedFile',
    type: UploadDto
  })
  @UseInterceptors(FileInterceptor('file',{
    storage: diskStorage({
      destination: process.cwd() + "/public/img",
      filename: (req, file, callback) => callback(null, new Date().getTime() + "_" + file.originalname)
    })
  }))
  @HttpCode(200)
  @Post('/users/upload-avatar/:id')
  async uploadAvatar(
    @UploadedFile() file: Express.Multer.File,
    @Param("id") id:string
  ){
    try {
      const filePath = file.path
      await this.usersService.uploadAvatar(id, filePath)
      return {filename: file.fieldname, path:filePath, id}
    } catch (error) {
      throw new InternalServerErrorException('Internet Server Error')
    }
  }
}
