import { Injectable, NotFoundException } from '@nestjs/common';
import { PrismaClient } from '@prisma/client';
import { UsersType } from './dto/user.dto';

@Injectable()
export class UsersService {
    prisma = new PrismaClient
    async getUsers(res){
       let data = await this.prisma.nguoiDung.findMany()
        res.json(data)
    }

    async createUsers(body: UsersType){
        let {name, email, pass_word,phone,birth_day,gender,role} = body

        let newData = {
            name,
            email,
            pass_word,
            phone,
            birth_day,
            gender,
            role, 
            avatar: null
        }

        let data = await this.prisma.nguoiDung.create({data: newData})
        return data
    }

    async deleteUser(id:string, res){
        await this.prisma.nguoiDung.delete({
            where: {
                id: Number(id)
            }
        })
        res.json({mess: 'Xóa thành công'})
    }

    async getUsersPagination(pageIndex:number, pageSize:number, keyword: string){
       let data =await this.prisma.nguoiDung.findMany({
            where: {
                name: keyword
            },
            skip: ((pageIndex - 1) * pageSize),
            take: pageSize
        })
        return data
    }

    async getUsersByID(id:string){
        let data = this.prisma.nguoiDung.findFirst({
            where: {
                id: Number(id)
            }
        })
        return data
    }


    async updateUsers(id:string, UsersType: UsersType){

        let userData = await this.prisma.viTri.findUnique({
            where:{
                id: Number(id)
            }
        })
    
        if(!userData) {
            throw new NotFoundException("Vị trí không tìm thấy")
        }
    
        return await this.prisma.nguoiDung.update({
            where: {
                id: Number(id)
            },
            data: UsersType
        })
        
    }

    async getUserByName(TenNguoiDung:string){
        let data = this.prisma.nguoiDung.findMany({
            where: {
                name: TenNguoiDung
            }
        })
        return data
    }

    async uploadAvatar(id:string, filePath: string){
        await this.prisma.nguoiDung.update({
            where: {id: Number(id)},
            data: {avatar: filePath}
        })
    }
}
