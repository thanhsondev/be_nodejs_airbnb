import { Injectable, NotFoundException, Res, InternalServerErrorException } from '@nestjs/common';
import { PrismaClient } from '@prisma/client';
import { LocationType } from './location-dto/location.dto';
import { json } from 'stream/consumers';


@Injectable()
export class LocationService {
    prisma = new PrismaClient()

   async getLocation(
    @Res() res
   ){
    let data =  await this.prisma.viTri.findMany()
    res.json(data)
    return data
    
  }

  async createLocation(body: LocationType, res){
    let {ten_vi_tri,tinh_thanh,quoc_gia,hinh_anh} = body

    let newData = {
        ten_vi_tri: ten_vi_tri,
        tinh_thanh: tinh_thanh,
        quoc_gia: quoc_gia,
        hinh_anh: hinh_anh
    }
    await this.prisma.viTri.create({data:newData})
    res.json({mess: "Tạo vị trí thành công"})
  }

  async getLocationPanigation (pageIndex: number = 1, pageSize: number=10, keyword: string) {

       const dataLocation = await this.prisma.viTri.findMany({
            where: {
                ten_vi_tri:{
                    contains: keyword
                }
            },
            skip: ((pageIndex - 1) * pageSize),
            take: pageSize
        })
        return dataLocation

  }

  async getLocationByID(id:string, res){
   let data = await this.prisma.viTri.findFirst({
        where: {
            id: Number(id)
        }
    })

    res.json(data)

  }

  async updateLocation(id:string, locationType:LocationType){

    let locationData = await this.prisma.viTri.findUnique({
        where:{
            id: Number(id)
        }
    })

    if(!locationData) {
        throw new NotFoundException("Vị trí không tìm thấy")
    }

    return await this.prisma.viTri.update({
        where:{
            id: Number(id)
        },
        data: locationType
    })

  }

  async deleteLocation(id:string){
    await this.prisma.viTri.delete({
        where: {
            id: Number(id)
        }
    })
  }

  async UploadImgLocation(id:string, filePath:string){
        await this.prisma.viTri.update({
          where: { id: Number(id) },
          data: { hinh_anh: filePath }
        });
    }
}
