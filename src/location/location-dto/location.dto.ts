import { ApiProperty } from "@nestjs/swagger"

export class LocationType {
    @ApiProperty()
    ten_vi_tri: string
    @ApiProperty()
    tinh_thanh: string
    @ApiProperty()
    quoc_gia: string
    @ApiProperty()
    hinh_anh: string
}

export class UploadImgLocationDto {
    @ApiProperty({type:'string', format: 'binary'})
    file: any
}