import { Body, Controller, DefaultValuePipe, Get, HttpCode, InternalServerErrorException, ParseIntPipe, Post, Query, Res, Param, Put, Delete, UseInterceptors, UploadedFile, UseGuards } from '@nestjs/common';
import { LocationService } from './location.service';
import { ApiBearerAuth, ApiBody, ApiConsumes, ApiQuery, ApiTags } from '@nestjs/swagger';
import { LocationType, UploadImgLocationDto } from './location-dto/location.dto';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { AuthGuard } from '@nestjs/passport';

@ApiBearerAuth()
@UseGuards(AuthGuard("jwt"))
@ApiTags("Vị Trí")
@Controller('location')
export class LocationController {
  constructor(private readonly locationService: LocationService) {}


  @HttpCode(200)
  @Get('/vi-tri')
  getLocation(@Res() res){
    try {
      let data = this.locationService.getLocation(res)
      return data
    } catch (error) {
      throw new InternalServerErrorException("Internet Server Error")
    }
  }

  @Post('/vi-tri')
  createLocation(
    @Body() body: LocationType,
    @Res() res
  ){
    try {
     let data = this.locationService.createLocation(body, res)
     return data
    } catch (error) {
      throw new InternalServerErrorException("Internet Server Error")
    }
  }


  @ApiQuery({
    name: 'keyword',
    required: false,
    type: String,
    description: 'The keyword for searching'
  })
  @HttpCode(200)
  @Get('/vi-tri/phan-trang-tim-kiem')
  getLocationPagination(
    @Query('pageIndex', new DefaultValuePipe(1), ParseIntPipe) pageIndex: number,
    @Query('pageSize', new DefaultValuePipe(10), ParseIntPipe) pageSize: number,
    @Query('keyword') keyword: string
  ){
    try {
      let data = this.locationService.getLocationPanigation(pageIndex, pageSize, keyword)
      return data
    } catch (error) {
      throw new InternalServerErrorException("Internet Server Error")
    }
  }

  @HttpCode(200)
  @Get("/vi-tri/:id")
  getLocationByID(
    @Param('id') id: string,
    @Res() res
  ){
    try {
      let data = this.locationService.getLocationByID(id, res)
      return data
      
    } catch (error) {
      throw new InternalServerErrorException("Internet Server Error")
    }
  }

  @HttpCode(200)
  @Put('/vi-tri/:id')
  UpdateLocation(
    @Param('id') id:string,
    @Body() locationType: LocationType 
  ){
    try {
      let newData = this.locationService.updateLocation(id,locationType )
      return newData
    } catch (error) {
      throw new InternalServerErrorException("Internet Server Error")
    }
  }

  @HttpCode(200)
  @Delete('/vi-tri/:id')
  deleteLocation(
    @Param('id') id:string
  ){
    try {
      this.locationService.deleteLocation(id)
    } catch (error) {
      throw new InternalServerErrorException("Internet Server Error")
    }
  }


  @ApiConsumes('multipart/form-data')
  @ApiBody({
    description: 'UploadedFile',
    type: UploadImgLocationDto
  })
  @UseInterceptors(FileInterceptor('file',{
    storage: diskStorage({
      destination: process.cwd() + "/public/img",
      filename: (req, file, callback) => callback(null, new Date().getTime() + "_" + file.originalname)
    })
  }))
  @HttpCode(200)
  @Post('/vi-tri/upload-hinh-vitri/:id')
 async uploadImgLocation(
    @UploadedFile() file: Express.Multer.File,
    @Param("id") id: string
  ){
    try {
      const filePath = file.path
     await this.locationService.UploadImgLocation( id, file.path)
      return {filename: file.fieldname, path:filePath, id}
    } catch (error) {
      throw new InternalServerErrorException("Internet Server Error")
    }
  }
}
